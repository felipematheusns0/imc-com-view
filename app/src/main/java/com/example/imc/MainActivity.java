package com.example.imc;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    EditText Peso, Altura;
    TextView ResultadoIMC;
    ImageView ResultadoImagem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void CalcularIMC(View v) {

        Peso = findViewById(R.id.txtPeso);
        Altura = findViewById(R.id.txtAltura);
        ResultadoIMC = findViewById(R.id.viewResultado);
        ResultadoImagem = findViewById(R.id.imageViewIMC);


        String PesoP = Peso.getText().toString();
        float finalPeso = Float.parseFloat(PesoP);

        String AlturaA = Altura.getText().toString();
        float finalAltura = Float.parseFloat(AlturaA);

        float AlturaFinal = finalAltura * finalAltura;
        float IMC = finalPeso / AlturaFinal;

        String ResultIMC = String.valueOf(IMC);

        ResultadoIMC.setText(ResultIMC);


        if (IMC >= 39) {
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade Morbida", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable = getResources().getDrawable(R.drawable.a);
            ResultadoImagem.setImageDrawable(drawable);
        }
        if ((IMC >= 29) || (IMC <= 38.9)) {
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade Moderada", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable = getResources().getDrawable(R.drawable.b);
            ResultadoImagem.setImageDrawable(drawable);
        }
        if ((IMC >= 24) || (IMC <= 28.9)) {
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade Leve", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable = getResources().getDrawable(R.drawable.c);
            ResultadoImagem.setImageDrawable(drawable);
        }
        if ((IMC >= 19) || (IMC <= 23.9)) {
            Toast toast = Toast.makeText(getApplicationContext(), "Normal", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable = getResources().getDrawable(R.drawable.d);
            ResultadoImagem.setImageDrawable(drawable);
        }
        if (IMC < 19) {
            Toast toast = Toast.makeText(getApplicationContext(), "Abaixo do Normal", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable = getResources().getDrawable(R.drawable.e);
            ResultadoImagem.setImageDrawable(drawable);
        }

    }

}
